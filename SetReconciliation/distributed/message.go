package distributed

import "../bloom_filter"
type Message interface {

}

type EstimatorMessage struct {
	nodeId int
	estimator *bloom_filter.Strata
}

func NewEstimatorMessage(nodeId int, estimator *bloom_filter.Strata) *EstimatorMessage{
	return &EstimatorMessage{nodeId, estimator}
}

type IBFMessage struct {
	nodeId int
	ibf *bloom_filter.InvertibleBloomFilter
}

func NewIBFMessage(nodeId int, ibf *bloom_filter.InvertibleBloomFilter) *IBFMessage {
	return &IBFMessage{nodeId, ibf}
}

type SBFMessage struct {
	nodeId int
	sbf *bloom_filter.StandardBloomFilter
}

func NewSBFMessage(nodeId int, sbf *bloom_filter.StandardBloomFilter) *SBFMessage {
	return &SBFMessage{nodeId, sbf}
}

type DataMessage struct {
	keys []int
}

func NewDataMessage(keys []int) *DataMessage {
	return &DataMessage{keys}
}

type DBFMessage struct {
	nodeId int
	dbf *bloom_filter.DistributedBloomFilter
}

func NewDBFMessage(nodeId int, dbf *bloom_filter.DistributedBloomFilter) *DBFMessage{
	return &DBFMessage{nodeId, dbf}
}