package distributed

import (
	"encoding/json"
	"io/ioutil"
	os "os"
)

var ConfigFile = "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\vertices_standard.json"

type Data struct {
	Nodes []struct {
		Neighbours []int `json:"neighbours"`
		ID         int   `json:"id"`
	} `json:"nodes"`
	Initial int     `json:"initial"`
	Fpp     float64 `json:"fpp"`
	Target  int     `json:"target"`
}

func ReadConfiguration() *Data{
	file, err := os.Open(ConfigFile)
	if err != nil {
		return nil
	}
	defer file.Close()

	var data Data
	byteValue, _ := ioutil.ReadAll(file)
	json.Unmarshal(byteValue, &data)
	return &data
}
