package distributed

import "../bloom_filter"

var channelsDbf []chan Message
var channelsDbfReceive []chan Message
var channelsDataDbf []chan Message

func InitChannelsDbf(numberOfNodes int) {
	channelsDbf = make([]chan Message, numberOfNodes)
	channelsDbfReceive = make([]chan Message, numberOfNodes)
	channelsDataDbf = make([]chan Message, numberOfNodes)
	for i := 0; i < numberOfNodes; i++ {
		channelsDbf[i] = make(chan Message)
		channelsDbfReceive[i] = make(chan Message)
		channelsDataDbf[i] = make(chan Message)
	}
}

type DBFSynchronizer struct {
	initialized bool
	size        int
	strategies  []bloom_filter.Strategy
	hashArray   []int
}

func NewDBFSynchronizer(fpp float64, n int) *DBFSynchronizer {
	hashArray := make([]int, 0)
	size := bloom_filter.CalculateBFSize(fpp, n)
	k := bloom_filter.CalculateNumberOfHashFunctions(size, n)
	strategies := bloom_filter.GenerateHashStrategies(k)
	return &DBFSynchronizer{false, size, strategies, hashArray}
}

func (sync *DBFSynchronizer) Sync(node *Node) {
	if !sync.initialized {
		sync.hashElements(node.elements)
		sync.initialized = true
	}
	nodeId := node.id
	neighbours := node.neighbours
	count := 0
	keysToAdd := make([]int, 0)
	for _, neighId := range neighbours {
		if nodeId < neighId {
			dbf := bloom_filter.NewDistributedBloomFilter(sync.size, nodeId, neighId, sync.strategies)
			dbf.Populate(sync.hashArray)
			channelsDbfReceive[neighId] <- NewDBFMessage(nodeId, dbf)
			message := <-channelsDbf[nodeId]
			dbfMessage := message.(*DBFMessage)
			keys := node.CalculateDifference(dbfMessage.dbf)
			message = <-channelsDataDbf[nodeId]
			dataMessage := message.(*DataMessage)
			channelsDataDbf[neighId] <- NewDataMessage(keys)
			keysToAdd = append(keysToAdd, dataMessage.keys...)
			count++
		}
	}

	for count < len(neighbours) {
		select {
		case message := <-channelsDbfReceive[nodeId]:
			dbfMessage := message.(*DBFMessage)
			neighId := dbfMessage.nodeId
			dbf := bloom_filter.NewDistributedBloomFilter(sync.size, nodeId, neighId, sync.strategies)
			dbf.Populate(sync.hashArray)
			channelsDbf[neighId] <- NewDBFMessage(nodeId, dbf)
			keys := node.CalculateDifference(dbfMessage.dbf)
			channelsDataDbf[neighId] <- NewDataMessage(keys)
		case message := <-channelsDataDbf[nodeId]:
			dataMessage := message.(*DataMessage)
			keysToAdd = append(keysToAdd, dataMessage.keys...)
			count++
		}
	}
	keys := node.AddElements(keysToAdd)
	sync.addHashValues(keys)
}

func (sync *DBFSynchronizer) hashElements(elements []bloom_filter.Element) {
	for _, v := range elements {
		sync.hashArray = append(sync.hashArray, bloom_filter.GetHash(v.GetId()))
	}
}

func (sync *DBFSynchronizer) addHashValues(keys []int) {
	for _, v := range keys {
		sync.hashArray = append(sync.hashArray, bloom_filter.GetHash(v))
	}
}
