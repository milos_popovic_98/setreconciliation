package distributed

import (
	"../bloom_filter"
)

var channelsSbf []chan Message
var channelsSbfReceive []chan Message
var channelsData []chan Message

func InitChannelsSBF(numberOfNodes int) {
	channelsSbf = make([]chan Message, numberOfNodes)
	channelsSbfReceive = make([]chan Message, numberOfNodes)
	channelsData = make([]chan Message, numberOfNodes)
	for i := 0; i < numberOfNodes; i++ {
		channelsSbf[i] = make(chan Message)
		channelsSbfReceive[i] = make(chan Message)
		channelsData[i] = make(chan Message)
	}
}

type SBFSynchronizer struct {
	initialized bool
	sbf         *bloom_filter.StandardBloomFilter
}

func NewSBFSynchronizer(fpp float64, n int) *SBFSynchronizer {
	m := bloom_filter.CalculateBFSize(fpp, n)
	return &SBFSynchronizer{false, bloom_filter.NewStandardBloomFilter(m, n)}
}

func (sync *SBFSynchronizer) Sync(node *Node) {
	if !sync.initialized {
		//fmt.Println("Sync initialized")
		sync.UpdateBF(GetElementIds(node.elements))
		sync.initialized = true
	}
	nodeId := node.id
	neighbours := node.neighbours
	count := 0
	keysToAdd := make([]int, 0)
	//fmt.Printf("Node %d\n", nodeId)
	for _, neighId := range neighbours {
		if nodeId < neighId {
			//fmt.Printf("Node %d sends to node %d\n", nodeId, neighId)
			channelsSbfReceive[neighId] <- NewSBFMessage(nodeId, sync.sbf)
			message := <-channelsSbf[nodeId]
			sbfMessage := message.(*SBFMessage)
			keys := node.CalculateDifference(sbfMessage.sbf)
			message = <-channelsData[nodeId]
			dataMessage := message.(*DataMessage)
			channelsData[neighId] <- NewDataMessage(keys)
			keysToAdd = append(keysToAdd, dataMessage.keys...)
			count++
		}
	}
	//fmt.Printf("Node receive %d\n", nodeId)
	for count < len(neighbours) {
		select {
		case message := <-channelsSbfReceive[nodeId]:
			sbfMessage := message.(*SBFMessage)
			neighId := sbfMessage.nodeId
			//fmt.Printf("Node %d receives from node %d\n", nodeId, neighId)
			channelsSbf[neighId] <- NewSBFMessage(nodeId, sync.sbf)
			keys := node.CalculateDifference(sbfMessage.sbf)
			channelsData[neighId] <- NewDataMessage(keys)
		case message := <-channelsData[nodeId]:
			dataMessage := message.(*DataMessage)
			keysToAdd = append(keysToAdd, dataMessage.keys...)
			count++
		}
	}
	keys := node.AddElements(keysToAdd)
	sync.UpdateBF(keys)
}

func (sync *SBFSynchronizer) UpdateBF(keys []int) {
	for _, key := range keys {
		sync.sbf.Update(bloom_filter.NewStringElement(key, string(key)))
	}
}
