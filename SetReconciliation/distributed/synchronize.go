package distributed

import (
	"../bloom_filter"
	"fmt"
)

var cellSize = 80
var c3 = 1.23
var numberOfHashFunctions = 3
var channels []chan Message
var channelsReceive []chan Message
var estimatorChannels []chan Message

func InitChannelsIbf(numberOfNodes int) {
	channels = make([]chan Message, numberOfNodes)
	channelsReceive = make([]chan Message, numberOfNodes)
	estimatorChannels = make([]chan Message, numberOfNodes)

	for i := 0; i < numberOfNodes; i++ {
		channels[i] = make(chan Message)
		channelsReceive[i] = make(chan Message)
		estimatorChannels[i] = make(chan Message)
	}
}

type Synchronizer interface {
	Sync(node *Node)
}

type IBFSynchronizer struct {
}

func NewIBFSynchronizer() *IBFSynchronizer {
	return &IBFSynchronizer{}
}

func (sync *IBFSynchronizer) Sync(node *Node) {
	estimator := node.getStrata()

	nodeId := node.id
	neighbours := node.neighbours
	count := 0
	keysToAdd := make([]int, 0)
	//fmt.Printf("Node %d has %d neigbours\n", nodeId, len(neighbours))
	for _, neighId := range neighbours {
		if nodeId < neighId {
			estimatorChannels[neighId] <- NewEstimatorMessage(nodeId, estimator)
			received := <-channels[nodeId]
			ibfMessage := received.(*IBFMessage)
			neighIbf := ibfMessage.ibf
			ibf := bloom_filter.NewInvertibleBloomFilter(neighIbf.Size, bloom_filter.GenerateHashStrategies(numberOfHashFunctions))
			ibf.Populate(node.elements)
			ibf.Subtract(neighIbf)
			a, b, _ := ibf.ListEntries()
			keysToAdd = append(keysToAdd, b...)
			count++
			channelsReceive[neighId] <- NewDataMessage(a)
		}
	}

	for count < len(neighbours) {
		select {
		case message := <-estimatorChannels[nodeId]:
			estimatorMessage := message.(*EstimatorMessage)
			estimator = node.getStrata()
			setDifference := estimator.Estimate(estimatorMessage.estimator)
			size := int(c3*float64(setDifference))
			fmt.Printf("%d\n", size)
			ibf := bloom_filter.NewInvertibleBloomFilter(size, bloom_filter.GenerateHashStrategies(numberOfHashFunctions))
			ibf.Populate(node.elements)
			channels[estimatorMessage.nodeId] <- NewIBFMessage(nodeId, ibf)
		case message := <-channelsReceive[nodeId]:
			dataMessage := message.(*DataMessage)
			keysToAdd = append(keysToAdd, dataMessage.keys...)
			count++
		}
	}
	node.AddElements(keysToAdd)
	//fmt.Printf("Node %d ended\n", nodeId)
}

func GetElementIds(elements []bloom_filter.Element) []int {
	keys := make([]int, len(elements))
	for i, v := range elements {
		keys[i] = v.GetId()
	}
	return keys
}
