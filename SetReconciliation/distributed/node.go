package distributed

import (
	"../bloom_filter"
	"fmt"
	"math"
	"math/rand"
	"sync"
)

var TARGET int

type Node struct {
	id           int
	fpp          float64
	targetNumberOfElements int
	elements     []bloom_filter.Element
	neighbours   []int
	synchronizer Synchronizer
}

func NewNode(id int, fpp float64, numberOfElements int, targetNumberOfElements int, neighbours []int, synchronizer Synchronizer) *Node {
	elements := make([]bloom_filter.Element, numberOfElements)
	perm := rand.Perm(targetNumberOfElements)
	for i := 0; i < numberOfElements; i++ {
		v := perm[i]
		elements[i] = bloom_filter.NewStringElement(v, string(v))
	}
	return &Node{id, fpp, targetNumberOfElements, elements, neighbours, synchronizer}
}

func (node *Node) AddElements(keys []int) []int {
	newKeys := make([]int, 0)
	for _, key := range keys {
		if !node.exists(key) {
			node.elements = append(node.elements, bloom_filter.NewStringElement(key, string(key)))
			newKeys = append(newKeys, key)
		}

	}
	//fmt.Printf("Node %d adds %d\n", node.id, len(newKeys))
	return newKeys
}

func (node *Node) getStrata() *bloom_filter.Strata {
	u:= int(math.Ceil(math.Log2(float64(node.targetNumberOfElements))))
	estimator := bloom_filter.NewStrata(cellSize, u)
	keys := GetElementIds(node.elements)
	estimator.Populate(keys)
	return estimator
}

func (node *Node) exists(key int) bool {
	if key >= node.targetNumberOfElements {
		return true
	}
	for _, v := range node.elements {
		if key == v.GetId() {
			return true
		}
	}
	return false
}

func (node *Node) CalculateDifference(bf bloom_filter.BloomFilter) []int {
	keys := make([]int, 0)
	for _, v := range node.elements {
		if !bf.Query(v) {
			keys = append(keys, v.GetId())
		}
	}
	return keys
}

func (node *Node) GetNumOfElements() int {
	return len(node.elements)
}

func (node *Node) Synchronize(wg *sync.WaitGroup) {
	defer wg.Done()
	node.synchronizer.Sync(node)
}

func (node* Node) Write() {
	for _, e := range node.elements {
		if e.GetId() > node.targetNumberOfElements {
			fmt.Printf(" %d", e.GetId())
		}
	}
}