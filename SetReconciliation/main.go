package main

import (
	"./distributed"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"
)

import "C"

var nodesStandard []*distributed.Node
var nodesInvertible []*distributed.Node
var nodesDistributed []*distributed.Node
var NumOfNodes = 25
var numOfIterations = 10
var outputFile = "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\result.txt"

func generate(data *distributed.Data) {
	nodesStandard = make([]*distributed.Node, NumOfNodes)
	nodesInvertible = make([]*distributed.Node, NumOfNodes)
	nodesDistributed = make([]*distributed.Node, NumOfNodes)

	//sbfSync := distributed.NewSBFSynchronizer()
	//ibfSync := distributed.NewIBFSynchronizer()
	//dbfSync := distributed.NewDBFSynchronizer()

	fpp := data.Fpp
	startNumber := data.Initial
	targetNumber := data.Target
	nodesData := data.Nodes
	NumOfNodes = len(nodesData)

	for i, node := range nodesData {
		nodesStandard[i] = distributed.NewNode(node.ID, fpp, startNumber, targetNumber, node.Neighbours, distributed.NewSBFSynchronizer(fpp, targetNumber))
		nodesInvertible[i] = distributed.NewNode(node.ID, fpp, startNumber, targetNumber, node.Neighbours, distributed.NewIBFSynchronizer())
		nodesDistributed[i] = distributed.NewNode(node.ID, fpp, startNumber, targetNumber, node.Neighbours, distributed.NewDBFSynchronizer(fpp, targetNumber))
	}
}

//export Simulate
func Simulate() {
	resultSbf := make([]int, numOfIterations*NumOfNodes)
	resultDbf := make([]int, numOfIterations*NumOfNodes)
	resultIbf := make([]int, numOfIterations*NumOfNodes)

	rand.Seed(time.Now().Unix())
	data := distributed.ReadConfiguration()
	generate(data)
	wg := sync.WaitGroup{}
	distributed.InitChannelsSBF(NumOfNodes)
	for i := 0; i < numOfIterations; i++ {
		wg.Add(NumOfNodes)
		for _, node := range nodesStandard {
			go node.Synchronize(&wg)
		}
		wg.Wait()
		//fmt.Printf("ITERATION %d", i)
		for index, node := range nodesStandard {
			//fmt.Printf("Node %d has %d\n", index, node.GetNumOfElements())
			resultSbf[i*NumOfNodes+index] = node.GetNumOfElements()
		}
	}

	distributed.InitChannelsDbf(NumOfNodes)
	for i := 0; i < numOfIterations; i++ {
		wg.Add(NumOfNodes)
		for _, node := range nodesDistributed {
			go node.Synchronize(&wg)
		}
		wg.Wait()
		//fmt.Printf("ITERATION %d", i)
		for index, node := range nodesDistributed {
			//fmt.Printf("Node %d has %d\n", index, node.GetNumOfElements())
			resultDbf[i*NumOfNodes+index] = node.GetNumOfElements()
		}
	}
	distributed.InitChannelsIbf(NumOfNodes)
	for i := 0; i < numOfIterations; i++ {
		wg.Add(NumOfNodes)
		for _, node := range nodesInvertible {
			go node.Synchronize(&wg)
		}
		wg.Wait()
		fmt.Printf("ITERATION %d", i)
		for index, node := range nodesInvertible {
			//fmt.Printf("Node %d has %d\n", i, node.GetNumOfElements())
			resultIbf[i*NumOfNodes+index] = node.GetNumOfElements()
		}
	}
	writeToFile(resultSbf, resultIbf, resultDbf)
}

func writeToFile(sbf []int, ibf []int, dbf []int) {
	file, _ := os.Create(outputFile)
	defer file.Close()
	for i := 0; i < numOfIterations; i++ {
		writeLine(file, sbf, i)
		writeLine(file, ibf, i)
		writeLine(file, dbf, i)
	}
}

func writeLine(file *os.File, sbf []int, line int) {
	for i := 0; i < NumOfNodes; i++ {
		file.WriteString(fmt.Sprintf(" %d", sbf[line * NumOfNodes + i]))
	}
	file.WriteString("\n")
}

//export Add
func Add(a, b int) int {
	return a + b
}

func main() {
	Simulate()
}
