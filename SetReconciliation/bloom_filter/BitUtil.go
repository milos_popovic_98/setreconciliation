package bloom_filter

import "math"

var ElemSize int = 32

func SetBit(array []int, val int) {
	offset := val % ElemSize
	array[val /ElemSize] |= 1 << offset
}

func QueryBit(array []int, val int) bool {
	offset := val % ElemSize
	return (array[val /ElemSize] & (1 << offset)) > 0
}

func RoundToNextInt(a int, b int) int {
	v := float64(a) / float64(b)
	c := math.Trunc(v)
	if v != c {
		return int(c + 1)
	} else {
		return int(c)
	}
}