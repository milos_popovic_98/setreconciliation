package bloom_filter


type KirschMitzenmacherHashStrategy struct {
	f1 Strategy
	f2 Strategy
	k  int
}

func NewKirschMitzenmacherHashStrategy(f1 Strategy, f2 Strategy, k int) *KirschMitzenmacherHashStrategy{
	return &KirschMitzenmacherHashStrategy{f1, f2, k}
}

func (strategy *KirschMitzenmacherHashStrategy) Hash(e Element) int {
	return strategy.Hash2(e.GetBody())
}

func (strategy *KirschMitzenmacherHashStrategy) Hash2(e []byte) int {
	hash1 := strategy.f1.Hash2(e)
	hash2 := strategy.f2.Hash2(e)
	k := strategy.k
	return hash1 + k * hash2
}