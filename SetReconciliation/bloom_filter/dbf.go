package bloom_filter

import (
	"math"
)

type DistributedBloomFilter struct {
	array []int
	size int
	hashValues []int
}

func NewDistributedBloomFilter(size int, id1 int, id2 int, strategies []Strategy) *DistributedBloomFilter {
	array := make([]int, RoundToNextInt(size, ElemSize))
	seed := id1 ^ id2
	hashValues := make([]int, len(strategies))
	for i, strategy := range strategies {
		hashValues[i] = strategy.Hash(NewStringElement(seed, string(seed)))
	}
	return &DistributedBloomFilter{array, size, hashValues}
}
func (dbf *DistributedBloomFilter) Populate(hashValues []int) {
	for _, val := range hashValues {
		for _, hash := range dbf.hashValues {
			SetBit(dbf.array, (val ^ hash) % dbf.size)
		}
	}
}

func (dbf *DistributedBloomFilter) Update(e Element) {
	val := GetHash(e.GetId())
	val = int(math.Abs(float64(val)))
	for _, hash := range dbf.hashValues {
		SetBit(dbf.array, (val ^ hash) % dbf.size)
	}
}

func (dbf *DistributedBloomFilter) Query(e Element) bool{
	val := GetHash(e.GetId())
	val = int(math.Abs(float64(val)))
	for _, hash := range dbf.hashValues {
		if !QueryBit(dbf.array, (val ^ hash) % dbf.size) {
			return false
		}
	}
	return true
}

