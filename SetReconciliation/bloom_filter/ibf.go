package bloom_filter

import (
	hash2 "hash"
	"hash/fnv"
)

var H hash2.Hash32 = fnv.New32()

type InvertibleBloomFilter struct {
	Size       int
	IdSet      []int
	HashSet    []int
	CountSet   []int
	strategies []Strategy
}

func NewInvertibleBloomFilter(size int, strategies []Strategy) *InvertibleBloomFilter {
	if size < 1 {
		size = 1
	}
	idSet := make([]int, size)
	hashSet := make([]int, size)
	countSet := make([]int, size)
	return &InvertibleBloomFilter{size, idSet, hashSet, countSet, strategies}
}

func (ibf *InvertibleBloomFilter) Update(e Element) {
	id := e.GetId()
	hash := GetHash(id)
	for _, strategy := range ibf.strategies {
		index := strategy.Hash(e) % ibf.Size
		ibf.IdSet[index] ^= id
		ibf.HashSet[index] ^= hash
		ibf.CountSet[index] += 1
	}
}

func (ibf *InvertibleBloomFilter) Query(e Element) bool {
	for _, strategy := range ibf.strategies {
		index := strategy.Hash(e)
		if ibf.CountSet[index] < 1 {
			return false
		}
	}
	return true
}

func (ibf *InvertibleBloomFilter) Delete(e Element) bool {
	for _, strategy := range ibf.strategies {
		index := strategy.Hash(e)
		ibf.IdSet[index] ^= e.GetId()
		ibf.HashSet[index] ^= GetHash(e.GetId())
		ibf.CountSet[index] -= 1
	}
	return true
}

func GetHash(id int) int {
	e := NewStringElement(id, string(id))
	H.Reset()
	H.Write(e.GetBody())
	return int(H.Sum32())
}

func (ibf *InvertibleBloomFilter) ListEntries() (a []int, b []int, success bool) {
	queue := make([]int, 0)
	a = make([]int, 0)
	b = make([]int, 0)
	for i := 0; i < ibf.Size; i++ {
		if ibf.isPure(i) {
			queue = append(queue, i)
		}
	}

	for len(queue) > 0 {
		index := queue[len(queue)-1]
		queue = queue[:len(queue)-1]
		if !ibf.isPure(index) {
			continue
		}
		key := ibf.IdSet[index]
		count := ibf.CountSet[index]
		if count > 0 {
			a = append(a, key)
		} else {
			b = append(b, key)
		}
		hash := GetHash(key)
		for _, strategy := range ibf.strategies {
			i := strategy.Hash(NewStringElement(key, string(key))) % ibf.Size
			ibf.IdSet[i] ^= key
			ibf.HashSet[i] ^= hash
			ibf.CountSet[i] -= count
			if ibf.isPure(i) {
				queue = append(queue, i)
			}
		}
	}
	for i := 0; i < ibf.Size; i++ {
		if ibf.IdSet[i] != 0 || ibf.CountSet[i] != 0 {
			success = false
			return
		}
	}
	success = true
	return
}

func (ibf *InvertibleBloomFilter) isPure(index int) bool {
	return (ibf.CountSet[index] == 1 || ibf.CountSet[index] == -1) && ibf.HashSet[index] == GetHash(ibf.IdSet[index])
}

func (ibf *InvertibleBloomFilter) Subtract(ibf2 *InvertibleBloomFilter) {
	if ibf.Size == ibf2.Size {
		for i := 0; i < ibf.Size; i++ {
			ibf.IdSet[i] ^= ibf2.IdSet[i]
			ibf.HashSet[i] ^= ibf2.HashSet[i]
			ibf.CountSet[i] -= ibf2.CountSet[i]
		}
	}
}

func (ibf *InvertibleBloomFilter) Populate(elements []Element) {
	for _, val := range elements {
		ibf.Update(val)
	}
}
