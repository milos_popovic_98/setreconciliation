package bloom_filter

type BloomFilter interface {
	Update(e Element)
	Query(e Element) bool
}


