package bloom_filter

import (
	"hash/crc32"
	"hash/fnv"
)

func GenerateHashStrategies(number int) []Strategy {
	if number < 1 {
		return nil
	}
	strategies := make([]Strategy, number)
	switch number {
	case 1:
		strategies[0] = NewSimpleHashStrategy(fnv.New32())
	case 2:
		strategies[0] = NewSimpleHashStrategy(fnv.New32())
		strategies[1] = NewSimpleHashStrategy(crc32.NewIEEE())
	default:
		strategies[0] = NewSimpleHashStrategy(fnv.New32())
		strategies[1] = NewSimpleHashStrategy(crc32.NewIEEE())
		for i:= 2; i < number; i++ {
			strategies[i] = NewKirschMitzenmacherHashStrategy(strategies[0], strategies[1], i)
		}
	}
	return strategies
}
