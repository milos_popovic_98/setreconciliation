package bloom_filter

import (
	"hash"
)

type Strategy interface {
	Hash(e Element) int
	Hash2(e []byte) int
}

type SimpleHashStrategy struct {
	function hash.Hash32
}

func NewSimpleHashStrategy(f hash.Hash32) *SimpleHashStrategy{
	return &SimpleHashStrategy{f}
}
func (strategy *SimpleHashStrategy) Hash(e Element) int {
	return strategy.Hash2(e.GetBody())
}

func (strategy *SimpleHashStrategy) Hash2(e []byte) int{
	hash := strategy.function
	hash.Reset()
	hash.Write(e)
	return int(hash.Sum32())
}