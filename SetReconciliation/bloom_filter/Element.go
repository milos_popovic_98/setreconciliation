package bloom_filter

type Element interface {
	GetBody() []byte
	GetId() int
}

type StringElement struct {
	body string
	id int
}

func NewStringElement(id int, body string) *StringElement {
	return &StringElement{body, id}
}

func (elem *StringElement) GetBody() []byte {
	return []byte(elem.body)
}

func (elem *StringElement) GetId() int {
	return elem.id
}