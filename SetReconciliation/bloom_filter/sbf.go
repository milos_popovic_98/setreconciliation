package bloom_filter

import (
	"math"
)

type StandardBloomFilter struct {
	array      []int
	size       int
	strategies []Strategy
}

func NewStandardBloomFilter(size int, numberOfElements int) *StandardBloomFilter {
	array := make([]int, RoundToNextInt(size, ElemSize))
	k := CalculateNumberOfHashFunctions(size, numberOfElements)
	strategies := GenerateHashStrategies(k)
	return &StandardBloomFilter{array, size, strategies}
}

func (sbf *StandardBloomFilter) Update(e Element) {
	for _, strategy := range sbf.strategies {
		hashVal := strategy.Hash(e) % sbf.size
		hashVal = int(math.Abs(float64(hashVal)))
		SetBit(sbf.array, hashVal)
	}
}
func (sbf *StandardBloomFilter) UpdateAll(elements []Element) {
	for _, e := range elements {
		sbf.Update(e)
	}
}

func (sbf *StandardBloomFilter) Query(e Element) bool {
	for _, strategy := range sbf.strategies {
		hashVal := strategy.Hash(e) % sbf.size
		hashVal = int(math.Abs(float64(hashVal)))
		if !(QueryBit(sbf.array, hashVal)) {
			return false
		}
	}
	return true
}

func (sbf *StandardBloomFilter) GetUniqueElements(bf BloomFilter, elements []Element) []Element {
	uniqueElements := make([]Element, 0, 10)
	for _, e := range elements {
		if !bf.Query(e) {
			uniqueElements = append(uniqueElements, e)
		}
	}
	return uniqueElements
}

func (sbf *StandardBloomFilter) getArray() []int {
	return sbf.array
}
