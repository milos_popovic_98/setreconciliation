package bloom_filter

var NumberOfHashFunctions = 3
type Strata struct {
	CellSize int
	U        int
	IBFSet   []*InvertibleBloomFilter
}

func NewStrata(cellSize int, u int) *Strata {
	IBFSet := make([]*InvertibleBloomFilter, u)
	return &Strata{cellSize, u, IBFSet}
}

func (s *Strata) Populate(keys []int) {
	for i := 0; i < s.U; i++ {
		s.IBFSet[i] = NewInvertibleBloomFilter(s.CellSize, GenerateHashStrategies(NumberOfHashFunctions))
	}

	for _, key := range keys {
		s.IBFSet[TrailingZeroes(GetHash(key), s.U-1)].Update(NewStringElement(key, string(key)))
	}
}

func (s *Strata) Estimate(s2 *Strata) int {
	count := 0
	for i := s.U - 1; i >= -1; i-- {
		if i < 0 {
			//fmt.Printf("Strata estimation 2: %d\n", count)
			return count
		}

		ibf1 := s.IBFSet[i]
		ibf2 := s2.IBFSet[i]
		ibf1.Subtract(ibf2)
		a, b, success := ibf1.ListEntries()
		if !success {
			if count == 0 {
				count++
			}
			return (2 << uint(i)) * count
		}
		count += len(a) + len(b)
	}
	return count
}

func CopyFilter(ibf * InvertibleBloomFilter) *InvertibleBloomFilter {
	copy := NewInvertibleBloomFilter(ibf.Size, ibf.strategies)
	for i := 0; i < ibf.Size; i++ {
		copy.IdSet[i] = ibf.IdSet[i]
		copy.CountSet[i] = ibf.CountSet[i]
		copy.HashSet[i] = ibf.HashSet[i]
	}
	return copy
}
func TrailingZeroes(id int, depth int) int {
	count := 0
	for count < depth {
		mask := 1 << count
		if (id & mask) == 0 {
			count++
		} else {
			return count
		}
	}
	return count
}
