package bloom_filter

import "math"

func CalculateBFSize(fpp float64, n int) int {
	return int(math.Ceil((float64(n) * math.Log(fpp)) / math.Log(1/math.Pow(2, math.Log(2)))))
}

func CalculateNumberOfHashFunctions(m int, n int) int{
	return int(math.Round((float64(m) / float64(n)) * math.Log(2)))
}